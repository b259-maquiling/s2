<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Divisible of five</h1>
    <?php printDivisibleOfFive()?>

    <h1>Array Manipulation</h1>
    <?php array_push($students, 'John Smith' ) ?>
    <pre><?php print_r($students);?></pre>
    <pre> <?php echo count($students) ?> </pre>
    <?php array_push($students, 'Jane Smith' ) ?>
    <pre><?php print_r($students);?></pre>
    <pre> <?php echo count($students) ?> </pre>
	<?php array_shift($students); ?>
    <pre><?php print_r($students);?></pre>
    <pre> <?php echo count($students) ?> </pre>
</body>
</html>