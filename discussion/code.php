<?php 

function whileLoop(){
    $count = 5;

    while($count !== 0) {
        echo $count . '<br/>';
        $count--;
    }
}

function doWhileLoop() {
    $count = 20;

    do {
        echo $count . '<br/>';
        $count--;
    } while($count > 20);
}

function forLoop(){
    for($count = 0; $count <= 20; $count++){
        echo $count . '<br/>';
    }
}

function modifiedForLoop(){
    for($count = 0; $count <=20; $count++){
        if($count % 2 === 0){
            continue;
        }

        echo $count . '<br/>';

        if($count > 10){
            break;
        }
    }
}

function whileLoopActivity() {
    $x = 1;

    while ($x <= 5) {
        echo $x . '<br/>';
        $x++;
    }
}

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927');
$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

$tasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

$gradePeriods = ['firstGrading' =>98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1, ];

$heroes = [
    ['iron man', 'thor', 'hulk'],
    ['wolverine', 'cyclops', 'jean grey'],
    ['batman', 'superman', 'wonder woman']
];

$ironManPowers=  [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

function searchBrand($brands, $brand){

    return(in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the arra";
}

$reversedGradePeriods = array_reverse($gradePeriods)
?>